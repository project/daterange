
The daterange module provides a programatic extension for date ranges that
can be used by other module developers.  And it creates two new filters for
node times:  'Node: Created Time Between' and 'Node: Changed Time Between'.

If jscalendar is installed and enabled the date fields are augmented with the
jscalendar button for selecting a date.

The supported operators are 'Custom date range', 'Outside date range',
'Before', 'After', 'within the past week', 'within the past month',
'within the past 3 months', 'within the past year'.  When the the operator
doesn't need both input fields, JavaScript attempts to undisplay the unused
fields.

The date fields understand the words 'today' and 'yesterday' and sutraction
of days, months, or years.  For example:

  'today-1' is 'yesterday'
  'today-7' is a week ago
  'today-1M' is a month ago
  'today-2Y' is two years ago

If you'd like additional operations or fewer operations, you can use the
hook_form_filter to hook the form and change the options.  Hint:  make sure
your custom modules weight is higher than this modules weight, so that your
module runs after the daterange module.

Installation
------------

1.  Install these files (daterange.module and daterange.js) in the modules
directory of your choice (for example, sites/yoursite.com/modules/daterange
or modules/daterange).
2.  Enable the daterange in the admin/modules page
3.  Create a view and add the 'Node: Created Time Between' or
'Node: Changed Time Between'

Author
------
Doug Green
doug@civicactions.com
douggreen@douggreenconsulting.com
